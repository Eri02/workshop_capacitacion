//Require Mongoose
const mongoose = require('mongoose');

//Define schema
const Schema = mongoose.Schema;

const TodoSchema = new Schema({
    label    : String,
    complete  : Boolean
});

module.exports = mongoose.model('Todo', TodoSchema);