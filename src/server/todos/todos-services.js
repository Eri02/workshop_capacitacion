const TodoModel = require('./todos-model');

    class TodoServices{
        //Get all todos
        static get(){
            return TodoModel.find();
        }
    }

module.exports = TodoServices;