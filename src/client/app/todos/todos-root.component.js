(function () {
    'use strict';

    angular
        .module('todos')
        .component('todosRoot', {
            controller: TodosController,
            templateUrl: "todos/todos-root.html"
        });

    TodosController.$inject = ['TodosService'];


    function TodosController(TodosService) {

        var _self = this;
        // var todoServ = TodosService;

        this.$onInit = function () {
            _self.todos = TodosService.getTodos();
        };

        this.addTodo = function ($event) {
            var label = $event.label;
            _self.todos = TodosService.addTodos(label);
        };

        this.completeTodo = function ($event) {
            var todo = $event.todo;
            _self.todos = TodosService.completeTodos(todo);
        };

        this.removeTodo = function ($event) {
            //var id = $event.todo.id;
            var todo = $event.todo;
            _self.todos = TodosService.removeTodos(todo);
        };
    }

})();