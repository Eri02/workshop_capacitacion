(function () {
    'use strict';

    angular
        .module('todos')
        .service('TodosService', TodosService);

    TodosService.$inject = [];

    function TodosService() {

        this.getTodos = getTodos;
        this.addTodos = addTodos;
        this.completeTodos = completeTodos;
        this.removeTodos = removeTodos;

     /*   this.listTodo = [{
            label: 'Eat pizza',
            id: 0,
            complete: true

        }, {
            label: 'Do some coding',
            id: 1,
            complete: true
        }, {
            label: 'Sleep',
            id: 2,
            complete: false
        }, {
            label: 'Print tickets',
            id: 3,
            complete: true
        }];
    */
        var _self = this;

        function getTodos() {
            return _self.listTodo;
        }

        function addTodos(label) {
            _self.listTodo.push({label:label,id: _self.listTodo.length + 1});
            return _self.listTodo;
        }

        function completeTodos(todo) {
            _self.listTodo = _self.listTodo.map(function (item) {
                return item.id === todo.id ? Object.assign({}, item, {complete: true}) : item
            });
            return _self.listTodo;
        }

        function removeTodos(todo) {
           _self.listTodo = _self.listTodo.filter(function(item) {
                return todo.id !== item.id;
            });
            return _self.listTodo;

        }
    }
})();
